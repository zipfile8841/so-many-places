ymaps.ready(init);
var map, places;

function init() {
    places = [];
    map = new ymaps.Map("map", {
        center: [59.93, 30.31],
        zoom: 10,
        controls: ['zoomControl']
    });

    var list = new Vue({
        el: '#list',
        data: {
            places: places
        },
        methods: {
            remove: function (place) {
                map.geoObjects.splice(this.places.indexOf(place), 1);
                places.splice(this.places.indexOf(place), 1);
            },

            go: function (place) {
                map.setCenter(map.geoObjects.get(this.places.indexOf(place)).geometry._coordinates);
                map.setZoom(15);
            },

            visit: function (place) {
                var color = place.visited ? '#C0C0C0' : '#0000FF';
                map.geoObjects.get(this.places.indexOf(place)).options.set('iconColor', color);
            }
        }
    })

    var add = new Vue({
        el: '#adder',
        data: {
            name: '',
            address: '',
        },
        methods: {
            add: function (name, address) {
                if (name !== '' && address !== '') {
                    var myGeocoder = ymaps.geocode(this.address, {
                        boundedBy: [[60.206664, 29.656951], [59.789945, 30.617726]],
                        strictBounds: true
                    });
                    myGeocoder.then(
                        function (res) {
                            if (res.metaData.geocoder.found === 1) {
                                var coords = res.geoObjects.properties._data.metaDataProperty.GeocoderResponseMetaData.Point.coordinates.reverse();
                                var newPlacemark = new ymaps.Placemark(coords, {
                                    hintContent: name,
                                    balloonContent: address,
                                }, {
                                    iconColor: '#0000FF'
                                });
                                places.push({name: name, address: address, visited: false})
                                map.geoObjects.add(newPlacemark);
                                map.setCenter(coords);
                                map.setZoom(15);
                                add.address = '';
                                add.name = '';
                            }
                            else {
                                alert('Неверно/неточно введен адрес');
                            }
                        },
                        function (err) {
                            alert('Неверно/неточно введен адрес');
                        }
                    );
                }
                else {
                    alert('Заполните все обязательные поля, помеченные *');
                }
            }
        }
    })
}

